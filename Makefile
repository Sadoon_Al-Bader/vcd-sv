CC=gcc
CARGS=-Wall -Werror -O2
OBJ=vcd
SRC=vcd.c
PREFIX=/usr/local/

all: comp
comp:
	$(CC) $(CARGS) -o $(OBJ) $(SRC)
clean:
	rm -rf ./vcd
install:
	cp ./$(OBJ) $(PREFIX)/bin/
uninstall:
	rm $(PREFIX)/bin/$(OBJ)
